const path = require('path');

module.exports = {
  mode: 'production',
  entry: {
    index: path.resolve(__dirname, 'src', 'index.js'),
  },
  output: {
    path: path.resolve(__dirname, 'build'),
    filename: 'index.js',
    library: {
      name: 'ReusableHooks',
      type: 'umd',
    },
    globalObject: 'this',
  },
  externals: {
    react: {
      amd: 'react',
      commonjs: 'react',
      commonjs2: 'react',
      root: 'React',
    },
  },
  optimization: {
    splitChunks: {
      chunks: 'all',
    },
    minimize: true,
  },
};
